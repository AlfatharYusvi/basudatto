DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS patient;
DROP TABLE IF EXISTS civitas_patient;
DROP TABLE IF EXISTS public_patient;
DROP TABLE IF EXISTS service;
DROP TABLE IF EXISTS policlinic;
DROP TABLE IF EXISTS specialization;
DROP TABLE IF EXISTS doctor;
DROP TABLE IF EXISTS treatment;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS shift;
DROP TABLE IF EXISTS doctor_shift;
DROP TABLE IF EXISTS checkup_schedule;
DROP TABLE IF EXISTS checkup_treatment;
DROP TABLE IF EXISTS medical_record;
DROP TABLE IF EXISTS cashier;
DROP TABLE IF EXISTS checkup_payment;
DROP TABLE IF EXISTS pharmacist;
DROP TABLE IF EXISTS pharmacy;
DROP TABLE IF EXISTS medicine;
DROP TABLE IF EXISTS prescription_order;
DROP TABLE IF EXISTS prescription_payment;
DROP TABLE IF EXISTS transaction_payment;

CREATE TABLE users(
username varchar(20) not null,
password varchar(20) not null,
email varchar(20) not null,
phone varchar(12) not null,
primary key (username)
);

create table patient(
patient_id INTEGER PRIMARY KEY AUTOINCREMENT,
dob date not null,
allergies varchar(50),
username varchar(20) not null,
foreign key (username) references users(username)
on update cascade on delete cascade
);

create table civitas_patient(
sso_id varchar(20) not null,
patient_id int not null,
primary key (sso_id),
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

create table public_patient(
id_card_no varchar(20) not null,
patient_id int not null,
primary key (id_card_no),
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

create table service(
id_service int not null,
service_name varchar(20) check (service_name='Policlinic' or service_name='Pharmacy'),
primary key (id_service)
);

create table policlinic(
policlinic_id int not null,
policlinic_name varchar(20) check (policlinic_name='General' or policlinic_name='Dentist' or policlinic_name='Counseling' or policlinic_name='Radiologist'),
service_id int not null,
primary key (policlinic_id),
foreign key (service_id) references service(id_service)
on update cascade on delete cascade
);

create table specialization(
specialization_id int not null,
specialization_name varchar(20) not null,
policlinic_id int not null,
primary key (specialization_id),
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade
);

create table doctor(
doctor_id INTEGER PRIMARY KEY AUTOINCREMENT,
doctor_name varchar(50) not null,
license_no varchar(20) not null,
specialization_id int not null,
username varchar(20) not null,
foreign key (username) references users(username),
foreign key (specialization_id) references specialization(specialization_id)
on update cascade on delete cascade
);

create table treatment(
treatment_id int not null,
treatment_name varchar(20) not null,
price float not null,
policlinic_id int not null,
primary key (treatment_id),
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade
);

create table room(
room_id int not null,
room_name varchar(20) not null,
policlinic_id int not null,
primary key (room_id),
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade
);

create table shift(
shift_id int not null,
day varchar(10) not null,
shift_type varchar(10) not null,
time_interval time not null,
primary key (shift_id)
);

create table doctor_shift(
doctor_shift_id int not null,
doctor_id int not null,
policlinic_id int not null,
room_id int not null,
primary key (doctor_shift_id),
foreign key (doctor_id) references doctor(doctor_id)
on update cascade on delete cascade,
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade,
foreign key (room_id) references room(room_id)
on update cascade on delete cascade
);

create table checkup_schedule(
checkup_schedule_id int not null,
doctor_shift_id int not null,
patient_id int not null,
date date not null,
primary key (checkup_schedule_id),
foreign key (doctor_shift_id) references doctor_shift(doctor_shift_id)
on update cascade on delete cascade,
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

create table checkup_treatment (
checkup_treatment_id int not null,
checkup_schedule_id int not null,
treatment_id int not null,
primary key (checkup_treatment_id),
foreign key (checkup_schedule_id) references checkup_schedule(checkup_schedule_id)
on update cascade on delete cascade,
foreign key (treatment_id) references treatment(treatment_id)
on update cascade on delete cascade
);

create table medical_record (
medical_record_id int not null,
checkup_schedule_id int not null,
symptmos varchar(50),
diagnosys varchar(50),
checkup_treatment_id int not null,
patient_id int not null,
primary key (medical_record_id),
foreign key (checkup_schedule_id) references checkup_schedule(checkup_schedule_id)
on update cascade on delete cascade,
foreign key (checkup_treatment_id) references checkup_treatment(checkup_treatment_id)
on update cascade on delete cascade,
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

CREATE TABLE CASHIER(
cashier_id INTEGER PRIMARY KEY AUTOINCREMENT,
username VARCHAR(20) NOT NULL,
FOREIGN KEY (username) REFERENCES _USER(username)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE CHECKUP_PAYMENT(
checkup_payment_id INT NOT NULL,
checkup_schedule_id INT NOT NULL,
id_public_card VARCHAR(20),
total_payment FLOAT NOT NULL,
status VARCHAR(10) NOT NULL,
recorded_by INT NOT NULL,
PRIMARY KEY (checkup_payment_id),
FOREIGN KEY (checkup_schedule_id) REFERENCES CHECKUP_SCHEDULE(checkup_schedule_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_public_card) REFERENCES PUBLIC_PATIENT(id_card_no)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (recorded_by) REFERENCES CASHIER(cashier_id)

ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PHARMACIST(
pharmacist_id INTEGER PRIMARY KEY AUTOINCREMENT,
license_no VARCHAR(20) NOT NULL,
username VARCHAR(20) NOT NULL,
FOREIGN KEY (username) REFERENCES _USER(username)
ON UPDATE CASCADE ON DELETE CASCADE
);

create table pharmacy(
pharmacy_id int not null,
license_no int not null,
primary key (pharmacy_id)
);

CREATE TABLE MEDICINE(
medicine_id INT NOT NULL,
medicine_name VARCHAR(50) NOT NULL,
price FLOAT NOT NULL,
stock INT NOT NULL,
pharmacy_id INT NOT NULL,
pharmacist_id INT NOT NULL,
PRIMARY KEY (medicine_id),
FOREIGN KEY (pharmacy_id) REFERENCES PHARMACY(pharmacy_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (pharmacist_id) REFERENCES PHARMACIST(pharmacist_id)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PRESCRIPTION_ORDER(
prescription_id INT NOT NULL,
patient_id INT NOT NULL,
medicine_id INT NOT NULL,
order_date DATE NOT NULL,
PRIMARY KEY (prescription_id),
FOREIGN KEY (patient_id) REFERENCES PATIENT(patient_id)

ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (medicine_id) REFERENCES MEDICINE(medicine_id)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PRESCRIPTION_PAYMENT(
prescription_payment_id INT NOT NULL,
prescription_id INT NOT NULL,
public_card_no varchar(20) NOT NULL,
total_payment FLOAT NOT NULL,
status varchar(10) NOT NULL,
PRIMARY KEY(prescription_payment_id),
FOREIGN KEY (prescription_id) REFERENCES PRESCRIPTION_ORDER(prescription_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (public_card_no) REFERENCES PUBLIC_PATIENT(id_card_no)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE TRANSACTION_PAYMENT(
transaction_payment_id INT NOT NULL,
checkup_payment_id INT NOT NULL,
prescription_payment_id INT NOT NULL,
public_card_no VARCHAR(20) NOT NULL,
total_payment FLOAT NOT NULL,
payment_date DATE NOT NULL,
status VARCHAR(10) NOT NULL,
recorded_by INT NOT NULL,
PRIMARY KEY (transaction_payment_id),
FOREIGN KEY (checkup_payment_id) REFERENCES CHECKUP_PAYMENT(checkup_payment_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (prescription_payment_id) REFERENCES PRESCRIPTION_PAYMENT(prescription_payment_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (public_card_no) REFERENCES PUBLIC_PATIENT(id_card_no)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (recorded_by) REFERENCES CASHIER(cashier_id)

ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO service (id_service, service_name) VALUES ('SV1', 'Policlinic');
INSERT INTO service (id_service, service_name) VALUES ('SV2', 'Pharmacy');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES ('PC1', 'General', 'SV1');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES ('PC2', 'Dentist', 'SV1');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES ('PC3', 'Counseling', 'SV1');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES ('PC4', 'Radiologist', 'SV1');
INSERT INTO specialization (specialization_id, specialization_name, policlinic_id) VALUES ('1', 'General', 'PC1');
INSERT INTO specialization (specialization_id, specialization_name, policlinic_id) VALUES ('2', 'Dentist', 'PC2');
INSERT INTO specialization (specialization_id, specialization_name, policlinic_id) VALUES ('3', 'Counseling', 'PC3');
INSERT INTO specialization (specialization_id, specialization_name, policlinic_id) VALUES ('4', 'Radiologist', 'PC4');